import os
import os.path as path
import sys
from bottle import *

app = Bottle()

@app.route('/')
def index():
    return static_file('index.html', root=path.join(os.getcwd(), 'views'))

@app.route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=path.join(os.getcwd(), 'static'))

if __name__=="__main__":
    run(app, host='0.0.0.0', port=8080, reloader=True)